#pragma once
#include "functions.hpp"
#include <fstream>
#include <iostream>

namespace saw {
    void Letter() {
        std::ifstream in("input.txt");
        char letter;
        int k = 0;
        int symbols[256];

        for (int i = 0; i < 256; i++)
            symbols[i] = 0;

        unsigned char c;
        while (in >> c)
        {
            symbols[c]++;
        }
        for (int i = 192; i < 256; i++)
        {
            if (symbols[i] > k) {
                k = symbols[i];
                letter = char(i);
            }
        }
        std::cout << letter << " - " << k << " ����" << std::endl;

    }
    void SmallLetters() {
        std::ifstream in("input.txt");
        int symbols[256];
        for (int i = 0; i < 256; i++)
            symbols[i] = 0;

        unsigned char c;
        while (in >> c)
        {
            if ((c >= 192) && (c <= 223))
                c += 32;
            symbols[c]++;
            std::cout << c;
        }
        std::cout << " " << std::endl;

    }
    void BigLetters() {
        std::ifstream in("input.txt");
        int symbols[256];
        for (int i = 0; i < 256; i++)
            symbols[i] = 0;

        unsigned char c;
        while (in >> c)
        {
            if ((c >= 224) && (c <= 255))
                c -= 32;
            symbols[c]++;
            std::cout << c;
        }
        std::cout << " " << std::endl;
    }
}
